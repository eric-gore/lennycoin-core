from ecdsa import SigningKey, VerifyingKey, NIST384p
from hashlib import sha256
import base64
from tinydb import TinyDB, Query, where

'''
This class' purpose is to allow the cli to create a default wallet for the person. 
This can be altered in the settings


'''


class Wallet:
    def __init__(self, publickey=None, privatekey=None):
        self.publickey = publickey
        self.privatekey = privatekey
        self.db = TinyDB('wallet.json', sort_keys=True, indent=4, separators=(',', ': '))

    def generateKeys(self):
        """
        This function generates key information for the local wallet.
        """
        sk = SigningKey.generate(curve=NIST384p)
        vk = sk.verifying_key
        sk_string = sk.to_string()
        vk_string = vk.to_string()
        self.publickey = base64.b64encode(vk_string).decode()
        self.privatekey = base64.b64encode(sk_string).decode()

    def createsig(self, data):
        get_key = SigningKey.from_string(base64.b64decode(self.privatekey), curve=NIST384p)
        signature = get_key.sign(data)
        sigpub = sha256(signature).hexdigest() + self.publickey
        return sigpub

    def store_outputs(self, block, computed_hash):
        UTXOs = self.db.table(str(self.publickey))

        block_name = block.compute_hash()

        blockdict = block.__dict__

        outputs = {}

        for i, trans in enumerate(blockdict['transactions']):
            transnum = 'trans' + str(i)
            outputs[transnum] = {}
            value = trans['value']
            outputs[transnum]['value'] = value
            outputs[transnum]['block_hash'] = computed_hash
            print(value)

        UTXOs.insert(outputs)

        return True

    def get_outputs(self, pubkey):
        UTXOs = self.db.table(str(pubkey))

        outputs = {}

        for i, UTXO in enumerate(UTXOs):
            outputs[i] = UTXO

        return outputs

    def get_balance(self):
        """
        This function is essentially gathering all transactions that are
        associated with the local wallet information.
        """

        wallet = self.db.table('UTXOs')

    def get_keys(self):
        keys = [self.privatekey, self.publickey]
        return keys
