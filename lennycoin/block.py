from hashlib import sha256
import json
from json import JSONEncoder


class Block:

    def __init__(self, height, chain, transactions, timestamp, previous_hash, nonce=0):
        self.height = height
        self.transactions = transactions
        self.chain = chain
        self.timestamp = timestamp
        self.previous_hash = previous_hash
        self.nonce = nonce

    def compute_hash(self):

        block_string = json.dumps(self.__dict__)

        return sha256(block_string.encode()).hexdigest()

    def toJson(self):
        return json.dumps(self, default=lambda o: o.__dict__, indent=4, sort_keys=True)
