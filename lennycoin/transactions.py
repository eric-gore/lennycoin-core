from hashlib import sha256
import json
import base64

from lennycoin.wallet import Wallet


class Transactions:
    def __init__(self, fr, to, value, inputs):
        self.fr = fr
        self.to = to
        self.value = value
        self.inputs = inputs

    @staticmethod
    def create_signatures(fr, to, value, wallet):
        data = fr + to + str(value)
        signature = wallet.createsig(base64.b64encode(str(data).encode()))

        return signature

    def create_transaction(self, wallet):
        trans = {}

        trans['from'] = self.fr
        trans['to'] = self.to
        trans['value'] = self.value
        trans['inputs'] = self.inputs
        trans['sig'] = self.create_signatures(self.fr, self.to, self.value, wallet)
        trans['id'] = self.create_id(trans)

        return trans

    @staticmethod
    def create_coinbase():
        trans = {}

        trans['from'] = 'coinbase'
        trans['to'] = 'miner'
        trans['value'] = 100
        trans['inputs'] = []
        trans['sig'] = 0
        trans['id'] = Transactions.create_id(trans)

        return trans

    @staticmethod
    def create_id(trans_data):
        data = json.dumps(trans_data).encode('utf-8')
        first_pass = sha256(data)
        second_pass = sha256(first_pass.hexdigest().encode('utf-8'))

        return second_pass.hexdigest()

    def toJson(self):
        print(self.__dict__)
        return json.dumps(self, default=lambda o: o.__dict__, indent=4, sort_keys=True)






