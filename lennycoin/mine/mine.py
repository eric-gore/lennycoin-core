import time

from lennycoin import block
from lennycoin.mine.proof_of_work import POW
from lennycoin.transactions import Transactions


class Mine:
    def __init__(self, blockchain):
        self.blockchain = blockchain
        self.UTXOs = []
        self.coinbase = Transactions.create_coinbase()
        self.UTXOs.append(self.coinbase)

    def add_new_transaction(self, transaction):
        self.UTXOs.append(transaction)

    def mine(self):
        """
        This function serves as an interface to add the pending
        transactions to the blockchain by adding them to the block
        and figuring out Proof Of Work.
        """
        if not self.UTXOs:
            return False

        last_block = self.blockchain.last_block

        new_block = block.Block(height=last_block.height + 1,
                                transactions=self.UTXOs,
                                chain=self.blockchain.chainname,
                                timestamp=time.time(),
                                previous_hash=self.blockchain.last_block.hash)

        proof = POW.proof_of_work(new_block)
        block_added = self.blockchain.add_block(new_block, proof)
        print(block_added)

        self.UTXOs = []
        block_info = [new_block, proof]

        return block_info
