from lennycoin.blockchain import Blockchain


class POW:
    def __init__(self):
        pass

    @staticmethod
    def confirm_validity(block, previous_block):
        if previous_block.height == 0:
            return True

        if previous_block.height + 1 != block.height:

            return False

        elif previous_block.compute_hash != block.previous_hash:

            return False

        elif block.timestamp <= previous_block.timestamp:

            return False

        return True

    @staticmethod
    def proof_of_work(block):
        """
        Function that tries different values of nonce to get a hash
        that satisfies our difficulty criteria.
        """
        block.nonce = 0

        computed_hash = block.compute_hash()
        while not computed_hash.startswith('0' * Blockchain.difficulty):
            block.nonce += 1
            computed_hash = block.compute_hash()

        return computed_hash
