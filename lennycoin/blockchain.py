import time

from lennycoin.block import Block
from lennycoin.transactions import Transactions


class Blockchain:
    def __init__(self, chainname):
        self.chain = []
        self.chainname = chainname
        self.create_genesis_block()

    def create_genesis_block(self):
        genesis_block = Block(0, self.chainname, [], time.time(), 0)
        genesis_block.hash = genesis_block.compute_hash()
        self.chain.append(genesis_block)

    @property
    def last_block(self):
        return self.chain[-1]

    difficulty = 2

    def add_block(self, block, proof):
        """
        A function that adds the block to the chain after verification.
        Verification includes:
        * Checking if the proof is valid.
        * The previous_hash referred in the block and the hash of latest block
          in the chain match.
        """
        previous_hash = self.last_block.hash

        if previous_hash != block.previous_hash:
            print("ERROR: Previous hash do not match!")
            return False

        if not self.confirm_validity(block, self.last_block):
            print("ERROR: Validity of block could not be verified!")
            return False

        block.hash = proof
        self.chain.append(block)
        return True

    @staticmethod
    def confirm_validity(block, previous_block):

        if previous_block.height == 0:
            return True

        if previous_block.height + 1 != block.height:

            return False

        elif previous_block.compute_hash != block.previous_hash:

            return False

        elif block.timestamp <= previous_block.timestamp:

            return False

        return True

    def verify_new_block(self, block):
        self.confirm_validity(block, previous_block=self.last_block)

        return True
