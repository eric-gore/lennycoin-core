class TransactionOutput:

    def __init__(self, id, recipient, value, parentTransactionID, block):
        # ID = the ID for transaction
        self.id = id
        # Recipient = the person getting the transaction
        self.recipient = recipient
        # Value = the ammount of lenny coin to send to that address
        self.value = value
        # The value of the Transaction that created the output
        self.parentTransactionID = parentTransactionID
        self.block = block

    def check_is_mine(self, pubkey):
        return pubkey == self.recipient
