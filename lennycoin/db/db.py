from tinydb import TinyDB, Query, where

class db_handler:
    def __init__(self, blockchain):
        self.blockchain = blockchain

    def insert_new_block(self, miner):
        db = TinyDB('blocks.json', sort_keys=True, indent=4, separators=(',', ': '))

        blocks = db.table('blocks')


        for block in self.blockchain.chain:
            blockdict = block.__dict__
            blockdict['miner'] = miner
            previously_entered = Query()
            if blocks.search(previously_entered['block']['height'] == block.height):
                pass
            else:
                blocks.insert({'block': blockdict})

        return True
