# Lennycoin Core
[![Lennycoin Core](https://github.com/eric-gore/lennycoin-core/actions/workflows/python-app.yml/badge.svg)](https://github.com/eric-gore/lennycoin-core/actions/workflows/python-app.yml)
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)

*Lennycoin is an attempt at a Python-based cryptocurrency.*

## Note
> This project is more for the fun of it and at this moment I have no real plans of this becoming a real thing.
> Anyone and everyone is more than welcome to fork or create pull request for this project.
> 
> *and yes my code is probably sloppy to most people but it'll slowly be cleaned up in the future*
> 

## Dependencies
  * ecdsa (https://github.com/tlsfuzzer/python-ecdsa)
  * pyans1 (https://github.com/etingof/pyasn1) - dependency
  * six (https://github.com/benjaminp/six) - compatibility library - dependency
  * tinydb (https://github.com/visualrobots/tinydb)

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)
