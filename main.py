from lennycoin import  blockchain
from lennycoin.db.db import db_handler
from lennycoin.mine.mine import Mine
from lennycoin.transactions import Transactions
from lennycoin.wallet import Wallet


def main():
    current_blockchain = blockchain.Blockchain("main")

    wallet_a = Wallet()
    wallet_b = Wallet()

    wallet_a.generateKeys()
    wallet_b.generateKeys()

    wakeys = wallet_a.get_keys()
    wbkeys = wallet_b.get_keys()

    wapublickey = wakeys[1]
    wbpublickey = wbkeys[1]

    test_transaction = Transactions(wapublickey, wbpublickey, 100, 0)

    miner = Mine(current_blockchain)

    miner.add_new_transaction(test_transaction.create_transaction(wallet_a))

    print(miner.UTXOs)

    new_block = miner.mine()

    print(new_block[0].transactions)

    if current_blockchain.verify_new_block(new_block[0]):
        wallet_b.store_outputs(new_block[0], new_block[1])

    for i in current_blockchain.chain:
        print(i.toJson())

    block_database = db_handler(current_blockchain).insert_new_block(wapublickey)

if __name__ == '__main__':
    main()
